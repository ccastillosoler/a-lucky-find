# A lucky find
Telegram bot to check the results of a raffle

## Description
This java application uses Selenium to get info from https://www.juegosonce.es in order to achieve the result and prizes of a given raffle

## Running
`java -jar -Dtelegram.api.key=$TELEGRAM_API_KEY a-lucky-find-1.0.jar `

## Use cases
### Available commands:
- /comprobar - check if your lottery ticket has been prized
- /resultado - get the winner number of the last raffle
- /premios - check the prizes list
- /ayuda - help about how to interact with the bot

### Examples
![aluckyfind_help](https://gitlab.com/ccastillosoler/a-lucky-find/-/wikis/uploads/c515bfe30ccb35751e80bbc1ad5cd28f/aluckyfind_help.PNG)
![aluckyfind_check](https://gitlab.com/ccastillosoler/a-lucky-find/-/wikis/uploads/4543b966f20581077ffbde6a5634315b/aluckyfind_check.PNG)
![aluckyfind_results](https://gitlab.com/ccastillosoler/a-lucky-find/-/wikis/uploads/cf91ab493bee75aa6fa7db448e1d7a28/aluckyfind_results.PNG)
![aluckyfind_prizes](https://gitlab.com/ccastillosoler/a-lucky-find/-/wikis/uploads/facceb9318d140f452b67206690386fe/aluckyfind_prizes.PNG)
