package com.donqui.aluckyfind;

import com.donqui.aluckyfind.bot.TelegramBot;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import org.telegram.telegrambots.updatesreceivers.DefaultBotSession;

@SpringBootApplication
public class ALuckyFindApplication implements CommandLineRunner {

    private static final Logger LOGGER = LoggerFactory.getLogger(ALuckyFindApplication.class);

    private final TelegramBot aLuckyFindBot;

    public static void main(String[] args) {
        SpringApplication.run(ALuckyFindApplication.class, args);
    }

    @Autowired
    public ALuckyFindApplication(TelegramBot aLuckyFindBot) {
        this.aLuckyFindBot = aLuckyFindBot;
    }

    @Override
    public void run(String... args) {
        try {
            TelegramBotsApi telegramBotsApi = new TelegramBotsApi(DefaultBotSession.class);
            telegramBotsApi.registerBot(aLuckyFindBot);
        } catch (TelegramApiException e) {
            LOGGER.error(e.getMessage(), e);
        }
    }
}