package com.donqui.aluckyfind.bot;

import com.donqui.aluckyfind.domain.session.Session;
import com.donqui.aluckyfind.domain.session.Sessions;
import com.donqui.aluckyfind.domain.ticket.Number;
import com.donqui.aluckyfind.domain.ticket.Series;
import com.donqui.aluckyfind.domain.ticket.Ticket;
import com.donqui.aluckyfind.domain.bot.Command;
import com.donqui.aluckyfind.domain.ticket.Extra;
import com.donqui.aluckyfind.domain.bot.Message;
import com.donqui.aluckyfind.provider.DataProvider;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.util.Locale;

@Service
public class TelegramBot extends TelegramLongPollingBot {

    private static final String BOT_NAME = "aLuckyFind";
    private static final Logger LOGGER = LoggerFactory.getLogger(TelegramBot.class);

    private final Locale locale;
    private final MessageSource messageSource;
    private final Sessions userSessions;
    private final String telegramBotApiKey;
    private final DataProvider dataProvider;

    public TelegramBot(@Value("${telegram.api.key}") String telegramBotApiKey,
                       @Value("${languageTag}") String languageTag,
                       MessageSource messageSource, Sessions userSessions, DataProvider dataProvider) {
        this.telegramBotApiKey = telegramBotApiKey;
        this.locale = Locale.forLanguageTag(languageTag);
        this.messageSource = messageSource;
        this.userSessions = userSessions;
        this.dataProvider = dataProvider;
    }

    @Override
    public String getBotUsername() {
        return BOT_NAME;
    }

    @Override
    public String getBotToken() {
        return telegramBotApiKey;
    }

    @Override
    public void onUpdateReceived(Update update) {
        if (update.hasCallbackQuery()) {
            LOGGER.info("Callback {} received from {}", update.getCallbackQuery().getData(), update.getCallbackQuery().getFrom().getFirstName());
            processCallback(update);
        } else if (update.hasMessage() && update.getMessage().hasText()) {
            LOGGER.info("Message {} received from {}", update.getMessage().getText(), update.getMessage().getChat().getFirstName());
            processInputMessage(update);
        }
    }

    private void processCallback(Update update) {
        long chatId = update.getCallbackQuery().getMessage().getChatId();

        processStep(chatId, update.getCallbackQuery().getData());
        if (isRequestCompleted(chatId)) {
            userSessions.remove(chatId);
        }
    }

    private void processInputMessage(Update update) {
        long chatId = update.getMessage().getChatId();
        String messageText = update.getMessage().getText();
        Command command = Command.get(messageText);

        switch (command) {
            case START:
            case HELP: {
                sendMessage(buildMessage(chatId, messageSource.getMessage(Message.WELCOME.getText(), null, locale)));
                break;
            }
            case CHECK: {
                processFirstStep(chatId);
                break;
            }
            case RESULT: {
                sendMessage(buildMessage(chatId, dataProvider.retrieveLastResult()));
                break;
            }
            case PRIZES: {
                sendMessage(buildMessage(chatId, dataProvider.retrievePrizes()));
                break;
            }
            case VALUES: {
                processStep(chatId, messageText);
                break;
            }
        }
    }

    private boolean isRequestCompleted(long chatId) {
        Ticket request = userSessions.getBy(chatId).getDataRequest();

        return request != null &&
                StringUtils.isNotBlank(request.getNumber()) &&
                StringUtils.isNotBlank(request.getSerie()) &&
                request.getExtra() != null &&
                request.getDateOption() != null;
    }

    private void processFirstStep(long chatId) {
        Ticket ticket = new Ticket();
        userSessions.reset(chatId, ticket);

        processStep(chatId, null);
    }

    private void processShortCut(long chatId, String receivedMessage) {
        if (StringUtils.isBlank(receivedMessage) || receivedMessage.split(" ").length < 2)
            throw new IllegalArgumentException();

        String[] keys = receivedMessage.split(" ");
        Number number = new Number(keys[0]);
        if (number.isNotValid()) throw new IllegalArgumentException();

        Series series = new Series(keys[1]);
        if (series.isNotValid()) throw new IllegalArgumentException();

        Extra extra = keys.length >= 3 ? Extra.parse(keys[2]) : Extra.UNKNOWN;
        Ticket request = new Ticket(number, series, extra);

        String result = dataProvider.checkResult(request.getNumber(), request.getSerie(), request.getExtra(), request.getDateOption());
        sendMessage(buildMessage(chatId, result));
    }

    private void processStep(long chatId, String receivedMessage) {
        try {
            Session userSession = userSessions.getBy(chatId);
            if (userSession.getDataRequest() == null) {
                processShortCut(chatId, receivedMessage);
                return;
            }
            sendMessage(userSession.handle(userSession.getDataRequest(), chatId, receivedMessage));
            userSession.nextState();
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            sendMessage(buildMessage(chatId, messageSource.getMessage(Message.NO_VALID_OPTION.getText(), null, locale)));
            userSessions.remove(chatId);
        }
    }

    private SendMessage buildMessage(long chatId, String messageTxt) {
        SendMessage message = new SendMessage();
        message.setChatId(String.valueOf(chatId));
        message.setText(messageTxt);

        return message;
    }

    private void sendMessage(SendMessage message) {
        try {
            execute(message);
        } catch (TelegramApiException e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

}
