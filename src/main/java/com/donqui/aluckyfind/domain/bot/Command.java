package com.donqui.aluckyfind.domain.bot;

import java.util.Arrays;

public enum Command {
    START("/start"),
    HELP("/ayuda"),
    CHECK("/comprobar"),
    RESULT("/resultado"),
    PRIZES("/premios"),
    VALUES("");

    private final String key;

    Command(String key) {
        this.key = key;
    }

    public static Command get(String key) {
        return Arrays.stream(Command.values())
                .filter(value -> value.key.equals(key))
                .findFirst()
                .orElse(VALUES);
    }
}
