package com.donqui.aluckyfind.domain.bot;

public enum Message {

    WELCOME("welcome"),
    NO_DATES_FOUND("no_dates_found"),
    NO_RESULTS_FOUND("no_results_found"),
    REQUEST_NUMBER("request_number"),
    REQUEST_SERIE("request_serie"),
    REQUEST_EXTRA("request_extra"),
    REQUEST_DATE("request_date"),
    NO_VALID_OPTION("no_valid_option"),
    YES("yes"),
    NO("no");

    private final String text;

    Message(String value) {
        this.text = value;
    }

    public String getText() {
        return text;
    }
}
