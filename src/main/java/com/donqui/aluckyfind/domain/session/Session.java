package com.donqui.aluckyfind.domain.session;

import com.donqui.aluckyfind.domain.ticket.Ticket;
import com.donqui.aluckyfind.domain.state.AbstractState;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;

public class Session {

    private long chatId;

    private Ticket ticket;

    private AbstractState state;

    public Session(long chatId, Ticket ticket, AbstractState state) {
        this.chatId = chatId;
        this.ticket = ticket;
        this.state = state;
    }

    public long getChatId() {
        return chatId;
    }

    public Ticket getDataRequest() {
        return ticket;
    }

    public void setDataRequest(Ticket ticket) {
        this.ticket = ticket;
    }

    public AbstractState getState() {
        return state;
    }

    public void setState(AbstractState state) {
        this.state = state;
    }

    public void nextState() {
        state.next(this);
    }

    public SendMessage handle(Ticket request, long chatId, String receivedMessage) {
        return state.handle(request, chatId, receivedMessage);
    }

}

