package com.donqui.aluckyfind.domain.session;

import com.donqui.aluckyfind.domain.ticket.Ticket;
import com.donqui.aluckyfind.domain.state.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;

@Component
public class Sessions {

    private WaitingForNumber firstState;

    private final Set<Session> sessions = new HashSet<>();

    @Autowired
    public void setFirstState(WaitingForNumber firstState) {
        this.firstState = firstState;
    }

    public Session getBy(long chatId) {
        return sessions.stream()
                .filter(session -> session.getChatId() == chatId)
                .findFirst()
                .orElse(new Session(chatId, null, firstState));
    }

    public void remove(long chatId) {
        sessions.remove(getBy(chatId));
    }

    public void reset(long chatId, Ticket ticket) {
        remove(chatId);

        sessions.add(new Session(chatId, ticket, firstState));
    }
}

