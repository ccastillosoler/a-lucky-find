package com.donqui.aluckyfind.domain.state;

import com.donqui.aluckyfind.domain.ticket.Ticket;
import com.donqui.aluckyfind.domain.session.Session;
import com.donqui.aluckyfind.domain.bot.Message;
import com.donqui.aluckyfind.provider.DataProvider;
import com.donqui.aluckyfind.service.DateService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

@Component
public abstract class AbstractState {

    private static final int MAX_BUTTONS_IN_A_ROW = 3;
    private static final int MAX_BUTTONS = 6;

    private final Locale locale;
    private final MessageSource messageSource;
    private final DataProvider dataProvider;
    private final DateService dateService;

    protected AbstractState(@Value("${languageTag}") String languageTag,
                            MessageSource messageSource, DataProvider dataProvider, DateService dateService) {
        this.locale = Locale.forLanguageTag(languageTag);
        this.messageSource = messageSource;
        this.dataProvider = dataProvider;
        this.dateService = dateService;
    }

    protected SendMessage requestExtra(long chatId) {
        InlineKeyboardMarkup markupInline = new InlineKeyboardMarkup();

        List<InlineKeyboardButton> rowInline = new ArrayList<>();
        rowInline.add(buildButton(messageSource.getMessage(Message.YES.getText(), null, locale), String.valueOf(1)));
        rowInline.add(buildButton(messageSource.getMessage(Message.NO.getText(), null, locale), String.valueOf(0)));

        markupInline.setKeyboard(Collections.singletonList(rowInline));

        return buildMessage(chatId, messageSource.getMessage(Message.REQUEST_EXTRA.getText(), null, locale), markupInline);
    }

    protected SendMessage requestDate(long chatId) {
        String availableDates = dataProvider.retrieveDates();
        String[] dates = availableDates.split("\n");
        if (dates.length == 0 || messageSource.getMessage(Message.NO_DATES_FOUND.getText(), null, locale).equals(availableDates)) {
            return buildMessage(chatId, messageSource.getMessage(Message.NO_DATES_FOUND.getText(), null, locale), null);
        }

        List<List<InlineKeyboardButton>> rowsInline = new ArrayList<>();
        List<InlineKeyboardButton> rowInline1 = new ArrayList<>();
        List<InlineKeyboardButton> rowInline2 = new ArrayList<>();

        for (int i = 0; i < MAX_BUTTONS; i++) {
            if (i < MAX_BUTTONS_IN_A_ROW) {
                rowInline1.add(buildButton(dateService.parse(dates[i]), String.valueOf(i)));
            } else {
                rowInline2.add(buildButton(dateService.parse(dates[i]), String.valueOf(i)));
            }
        }
        rowsInline.add(rowInline1);
        rowsInline.add(rowInline2);

        InlineKeyboardMarkup markupInline = new InlineKeyboardMarkup();
        markupInline.setKeyboard(rowsInline);

        return buildMessage(chatId, messageSource.getMessage(Message.REQUEST_DATE.getText(), null, locale), markupInline);
    }

    protected SendMessage getResult(long chatId, Ticket request) {
        String message = dataProvider.checkResult(request.getNumber(), request.getSerie(), request.getExtra(), request.getDateOption());

        return buildMessage(chatId, message, null);
    }

    private InlineKeyboardButton buildButton(String text, String callbackData) {
        InlineKeyboardButton button = new InlineKeyboardButton();
        button.setText(text);
        button.setCallbackData(callbackData);
        return button;
    }

    protected SendMessage buildMessage(long chatId, String messageTxt, InlineKeyboardMarkup markupInline) {
        SendMessage message = new SendMessage();
        message.setChatId(String.valueOf(chatId));
        message.setText(messageTxt);
        message.setReplyMarkup(markupInline);

        return message;
    }

    protected String getMessage(String messageText) {
        return messageSource.getMessage(messageText, null, locale);
    }

    public void next(Session session) {
    }

    public SendMessage handle(Ticket request, long chatId, String receivedMessage) {
        return null;
    }

}