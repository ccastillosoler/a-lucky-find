package com.donqui.aluckyfind.domain.state;

import com.donqui.aluckyfind.domain.ticket.Ticket;
import com.donqui.aluckyfind.domain.session.Session;
import com.donqui.aluckyfind.provider.DataProvider;
import com.donqui.aluckyfind.service.DateService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;

@Component
public class ReceivedDate extends AbstractState {

    public ReceivedDate(@Value("${languageTag}") String languageTag,
                        MessageSource messageSource,
                        DataProvider dataProvider,
                        DateService dateService) {
        super(languageTag, messageSource, dataProvider, dateService);
    }

    @Override
    public void next(Session session) {
    }

    @Override
    public SendMessage handle(Ticket request, long chatId, String receivedMessage) {
        if (request == null) throw new IllegalArgumentException();

        request.setDateOption(Integer.parseInt(receivedMessage));
        return getResult(chatId, request);
    }

}