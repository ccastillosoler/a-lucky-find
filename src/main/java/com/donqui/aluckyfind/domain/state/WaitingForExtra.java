package com.donqui.aluckyfind.domain.state;

import com.donqui.aluckyfind.domain.session.Session;
import com.donqui.aluckyfind.domain.ticket.Series;
import com.donqui.aluckyfind.domain.ticket.Ticket;
import com.donqui.aluckyfind.provider.DataProvider;
import com.donqui.aluckyfind.service.DateService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;

@Component
public class WaitingForExtra extends AbstractState {

    private final WaitingForDate waitingForDate;

    public WaitingForExtra(@Value("${languageTag}") String languageTag,
                           MessageSource messageSource,
                           DataProvider dataProvider,
                           DateService dateService,
                           WaitingForDate waitingForDate) {
        super(languageTag, messageSource, dataProvider, dateService);
        this.waitingForDate = waitingForDate;
    }

    @Override
    public void next(Session session) {
        session.setState(waitingForDate);
    }

    @Override
    public SendMessage handle(Ticket request, long chatId, String receivedMessage) {
        if (request == null) throw new IllegalArgumentException();

        Series series = new Series(receivedMessage);
        if (series.isNotValid()) throw new IllegalArgumentException();

        request.setSerie(series);

        return requestExtra(chatId);
    }
}