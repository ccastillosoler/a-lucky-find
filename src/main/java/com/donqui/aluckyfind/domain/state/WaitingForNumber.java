package com.donqui.aluckyfind.domain.state;

import com.donqui.aluckyfind.domain.ticket.Ticket;
import com.donqui.aluckyfind.domain.session.Session;
import com.donqui.aluckyfind.domain.bot.Message;
import com.donqui.aluckyfind.provider.DataProvider;
import com.donqui.aluckyfind.service.DateService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;

@Component
public class WaitingForNumber extends AbstractState {

    private final WaitingForSerie waitingForSerie;

    public WaitingForNumber(@Value("${languageTag}") String languageTag,
                            MessageSource messageSource,
                            DataProvider dataProvider,
                            DateService dateService,
                            WaitingForSerie waitingForSerie) {
        super(languageTag, messageSource, dataProvider, dateService);
        this.waitingForSerie = waitingForSerie;
    }

    @Override
    public void next(Session session) {
        session.setState(waitingForSerie);
    }

    @Override
    public SendMessage handle(Ticket request, long chatId, String receivedMessage) {
        return buildMessage(chatId, getMessage(Message.REQUEST_NUMBER.getText()), null);
    }

}