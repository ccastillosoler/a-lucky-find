package com.donqui.aluckyfind.domain.state;

import com.donqui.aluckyfind.domain.session.Session;
import com.donqui.aluckyfind.domain.ticket.Number;
import com.donqui.aluckyfind.domain.ticket.Ticket;
import com.donqui.aluckyfind.domain.bot.Message;
import com.donqui.aluckyfind.provider.DataProvider;
import com.donqui.aluckyfind.service.DateService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;

@Component
public class WaitingForSerie extends AbstractState {

    private final WaitingForExtra waitingForExtra;

    public WaitingForSerie(@Value("${languageTag}") String languageTag,
                           MessageSource messageSource,
                           DataProvider dataProvider,
                           DateService dateService,
                           WaitingForExtra waitingForExtra) {
        super(languageTag, messageSource, dataProvider, dateService);
        this.waitingForExtra = waitingForExtra;
    }

    @Override
    public void next(Session session) {
        session.setState(waitingForExtra);
    }

    @Override
    public SendMessage handle(Ticket request, long chatId, String receivedMessage) {
        if (request == null) throw new IllegalArgumentException();

        Number number = new Number(receivedMessage);
        if (number.isNotValid()) throw new IllegalArgumentException();

        request.setNumber(number);

        return buildMessage(chatId, getMessage(Message.REQUEST_SERIE.getText()), null);
    }
}