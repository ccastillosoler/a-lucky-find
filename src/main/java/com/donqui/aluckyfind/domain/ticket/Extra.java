package com.donqui.aluckyfind.domain.ticket;

import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;

public enum Extra {

    SELECTED("1", true),
    NOT_SELECTED("0", false),
    UNKNOWN(null, false);

    private final String code;
    private final boolean selected;

    public String getCode() {
        return code;
    }

    public boolean isSelected() {
        return selected;
    }

    Extra(String code, boolean selected) {
        this.code = code;
        this.selected = selected;
    }

    public static Extra parse(String text) {
        if (StringUtils.isBlank(text)) return UNKNOWN;

        return Arrays.stream(Extra.values())
                .filter(extra -> extra.getCode().equals(text))
                .findFirst()
                .orElse(UNKNOWN);
    }

}
