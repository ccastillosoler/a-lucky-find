package com.donqui.aluckyfind.domain.ticket;

import org.apache.commons.lang3.StringUtils;

public class Number {

    private final String value;

    public Number(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public boolean isNotValid() {
        if (StringUtils.isBlank(this.value)) return true;
        try {
            int number = Integer.parseInt(this.value);
            if (number < 0 || number > 99999) return true;
        } catch (NumberFormatException e) {
            return true;
        }

        return false;
    }
}
