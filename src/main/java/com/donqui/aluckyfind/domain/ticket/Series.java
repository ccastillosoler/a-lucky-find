package com.donqui.aluckyfind.domain.ticket;

import org.apache.commons.lang3.StringUtils;

public class Series {

    private final String value;

    public Series(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public boolean isNotValid() {
        if (StringUtils.isBlank(this.value)) return true;
        try {
            int serieInput = Integer.parseInt(this.value);
            if (serieInput < 1 || serieInput > 55) return true;
        } catch (NumberFormatException e) {
            return true;
        }

        return false;
    }
}
