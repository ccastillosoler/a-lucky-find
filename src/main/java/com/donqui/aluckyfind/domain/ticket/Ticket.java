package com.donqui.aluckyfind.domain.ticket;

public class Ticket {

    private Number number;
    private Series series;
    private Extra extra;
    private Integer dateOption;

    public Ticket() {
    }

    public Ticket(Number number, Series series, Extra extra) {
        this.number = number;
        this.series = series;
        this.extra = extra;
    }

    public String getNumber() {
        return number != null ? number.getValue() : null;
    }

    public void setNumber(Number number) {
        this.number = number;
    }

    public String getSerie() {
        return series != null ? series.getValue() : null;
    }

    public void setSerie(Series series) {
        this.series = series;
    }

    public Boolean getExtra() {
        return extra != null && extra.isSelected();
    }

    public void setExtra(Extra extra) {
        this.extra = extra;
    }

    public Integer getDateOption() {
        return dateOption;
    }

    public void setDateOption(Integer dateOption) {
        this.dateOption = dateOption;
    }

}

