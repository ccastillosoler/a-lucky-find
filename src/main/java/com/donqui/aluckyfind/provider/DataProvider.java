package com.donqui.aluckyfind.provider;

import com.donqui.aluckyfind.domain.bot.Message;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.support.ui.Select;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import java.util.Locale;
import java.util.stream.Collectors;

@Service
public class DataProvider {

    private static final Logger LOGGER = LoggerFactory.getLogger(DataProvider.class);

    private final String checkUrl;
    private final String resultUrl;
    private final String ticketXpath;
    private final String prizesClass;
    private final String prizesTittle;
    private final String acceptButton;
    private final String numberField;
    private final String serieField;
    private final String extraLabel;
    private final String submitButton;
    private final String datesSelect;
    private final String responseXpath;

    private final Locale locale;
    private final WebDriver driver;
    private final MessageSource messageSource;

    public DataProvider(@Value("${checkUrl}") String checkUrl,
                        @Value("${resultUrl}") String resultUrl,
                        @Value("${acceptButton}") String acceptButton,
                        @Value("${numberField}") String numberField,
                        @Value("${serieField}") String serieField,
                        @Value("${extraLabel}") String extraLabel,
                        @Value("${submitButton}") String submitButton,
                        @Value("${datesSelect}") String datesSelect,
                        @Value("${responseXpath}") String responseXpath,
                        @Value("${ticketXpath}") String ticketXpath,
                        @Value("${prizesClass}") String prizesClass,
                        @Value("${prizesTittle}") String prizesTittle,
                        @Value("${languageTag}") String languageTag,
                        MessageSource messageSource) {
        this.checkUrl = checkUrl;
        this.resultUrl = resultUrl;
        this.acceptButton = acceptButton;
        this.numberField = numberField;
        this.serieField = serieField;
        this.extraLabel = extraLabel;
        this.submitButton = submitButton;
        this.datesSelect = datesSelect;
        this.responseXpath = responseXpath;
        this.ticketXpath = ticketXpath;
        this.prizesClass = prizesClass;
        this.prizesTittle = prizesTittle;
        this.locale = Locale.forLanguageTag(languageTag);
        this.driver = new HtmlUnitDriver(false);
        this.messageSource = messageSource;
    }

    public String retrieveDates() {
        try {
            driver.get(checkUrl);
            return driver.findElement(By.id(datesSelect)).getText();
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            return messageSource.getMessage(Message.NO_DATES_FOUND.getText(), null, locale);
        }
    }

    public String retrieveLastResult() {
        try {
            driver.get(resultUrl);
            return driver.findElement(By.xpath(ticketXpath)).getText();
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            return messageSource.getMessage(Message.NO_RESULTS_FOUND.getText(), null, locale);
        }
    }

    public String retrievePrizes() {
        try {
            driver.get(resultUrl);
            return driver.findElements(By.className(prizesClass)).stream()
                    .map(prizes -> "- " + prizes.getAttribute(prizesTittle) + ":\n" + prizes.getText())
                    .collect(Collectors.joining("\n"));
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            return messageSource.getMessage(Message.NO_RESULTS_FOUND.getText(), null, locale);
        }
    }

    public String checkResult(String number, String series, Boolean extraCheck, Integer dateOption) {
        try {
            driver.get(checkUrl);
            driver.findElement(By.id(acceptButton)).click();
            setDate(dateOption);
            setExtra(extraCheck);
            setInputText(number, numberField);
            setInputText(series, serieField);
            driver.findElement(By.className(submitButton)).click();

            return driver.findElement(By.cssSelector(responseXpath)).getText();
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            return messageSource.getMessage(Message.NO_RESULTS_FOUND.getText(), null, locale);
        }
    }

    private void setInputText(String input, String inputField) {
        driver.findElement(By.id(inputField)).click();
        driver.findElement(By.id(inputField)).sendKeys(input);
    }

    private void setExtra(Boolean extraCheck) {
        if (extraCheck == null || !extraCheck) return;

        driver.findElement(By.id(extraLabel)).click();
    }

    private void setDate(Integer dateOption) {
        if (dateOption == null) return;

        Select select = new Select(driver.findElement(By.id(datesSelect)));
        select.selectByIndex(dateOption);
    }

}
