package com.donqui.aluckyfind.service;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

@Service
public class DateService {

    private final DateTimeFormatter longFormat;
    private final DateTimeFormatter shortFormat;


    public DateService(@Value("${languageTag}") String languageTag) {
        Locale locale = Locale.forLanguageTag(languageTag);
        this.longFormat = DateTimeFormatter.ofPattern("EEEE, dd 'de' MMMM 'de' yyyy", locale);
        this.shortFormat = DateTimeFormatter.ofPattern("dd/MM/yy", locale);
    }

    public String parse(String dateString) {
        if (StringUtils.isEmpty(dateString)) return null;

        try {
            LocalDate localDate = LocalDate.parse(dateString, longFormat);
            return localDate.format(shortFormat);
        } catch (Exception e) {
            return null;
        }
    }
}
