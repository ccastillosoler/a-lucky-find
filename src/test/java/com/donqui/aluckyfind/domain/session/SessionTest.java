package com.donqui.aluckyfind.domain.session;

import com.donqui.aluckyfind.domain.state.*;
import com.donqui.aluckyfind.domain.ticket.Ticket;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.instanceOf;

@SpringBootTest
@TestPropertySource(locations = {"classpath:application-test.properties"})
public class SessionTest {

    @Autowired
    private WaitingForNumber waitingForNumber;

    @Test
    public void testNextStates() {
        Session session = new Session(1, new Ticket(), waitingForNumber);

        assertThat(session.getState(), instanceOf(WaitingForNumber.class));
        session.nextState();

        assertThat(session.getState(), instanceOf(WaitingForSerie.class));
        session.nextState();

        assertThat(session.getState(), instanceOf(WaitingForExtra.class));
        session.nextState();

        assertThat(session.getState(), instanceOf(WaitingForDate.class));
        session.nextState();

        assertThat(session.getState(), instanceOf(ReceivedDate.class));
    }

}