package com.donqui.aluckyfind.domain.state;

import com.donqui.aluckyfind.domain.ticket.Ticket;
import com.donqui.aluckyfind.domain.ticket.Extra;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@TestPropertySource(locations = {"classpath:application-test.properties"})
public class StateHandleTest {

    @Autowired
    private WaitingForNumber waitingForNumber;
    @Autowired
    private WaitingForSerie waitingForSerie;
    @Autowired
    private WaitingForExtra waitingForExtra;
    @Autowired
    private WaitingForDate waitingForDate;
    @Autowired
    private ReceivedDate receivedDate;

    @Test
    public void testWaitingForNumberHandler() {
        SendMessage message = waitingForNumber.handle(new Ticket(), 1, null);
        assertEquals(message.getText(), "¿Cuál es el número del cupón?");
    }

    @Test
    public void testWaitingForSerieHandler() {
        Ticket request = new Ticket();

        SendMessage message = waitingForSerie.handle(request, 1, "12345");

        assertEquals(message.getText(), "¿Cuál es la serie?");
        assertEquals(request.getNumber(), "12345");
    }

    @Test
    public void testWaitingForExtraHandler() {
        Ticket request = new Ticket();

        SendMessage message = waitingForExtra.handle(request, 1, "50");

        assertEquals(message.getText(), "¿Has comprado el cupón con la paga?");
        assertEquals(request.getSerie(), "50");
    }

    @Test
    public void testWaitingForDateHandler() {
        Ticket request = new Ticket();

        SendMessage message = waitingForDate.handle(request, 1, Extra.SELECTED.getCode());

        assertEquals(message.getText(), "¿Cuál es la fecha del sorteo?");
        assertEquals(request.getExtra(), Boolean.TRUE);
    }

    @Test
    public void testReceivedDateHandler() {
        Ticket request = new Ticket();

        receivedDate.handle(request, 1, "0");

        assertEquals(request.getDateOption(), 0);
    }

}