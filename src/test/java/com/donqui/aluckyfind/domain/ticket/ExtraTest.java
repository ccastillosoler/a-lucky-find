package com.donqui.aluckyfind.domain.ticket;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class ExtraTest {

    @Test
    void testExtraWithNullInput() {
        Extra extra = Extra.parse(null);

        assertFalse(extra.isSelected());
    }

    @Test
    void testExtraWithEmptyInput() {
        Extra extra = Extra.parse("");

        assertFalse(extra.isSelected());
    }

    @Test
    void testExtraWithZeroInput() {
        Extra extra = Extra.parse("0");

        assertFalse(extra.isSelected());
    }

    @Test
    void testExtraWithOneInput() {
        Extra extra = Extra.parse("1");

        assertTrue(extra.isSelected());
    }

}