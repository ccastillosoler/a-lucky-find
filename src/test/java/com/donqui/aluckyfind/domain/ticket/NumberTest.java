package com.donqui.aluckyfind.domain.ticket;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class NumberTest {

    @Test
    void testNumberWithNullInput() {
        Number number = new Number(null);

        assertTrue(number.isNotValid());
    }

    @Test
    void testNumberWithEmptyInput() {
        Number number = new Number("");

        assertTrue(number.isNotValid());
    }

    @Test
    void testNumberWithNegativeInput() {
        Number number = new Number("-1");

        assertTrue(number.isNotValid());
    }

    @Test
    void testNumberWithTooBigInput() {
        Number number = new Number("100000");

        assertTrue(number.isNotValid());
    }

    @Test
    void testNumberWithValidInput() {
        Number number = new Number("12345");

        assertFalse(number.isNotValid());
    }

}