package com.donqui.aluckyfind.domain.ticket;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class SeriesTest {

    @Test
    void testSeriesWithNullInput() {
        Series series = new Series(null);

        assertTrue(series.isNotValid());
    }

    @Test
    void testSeriesWithEmptyInput() {
        Series series = new Series("");

        assertTrue(series.isNotValid());
    }

    @Test
    void testSeriesWithNegativeInput() {
        Series series = new Series("-1");

        assertTrue(series.isNotValid());
    }

    @Test
    void testSeriesWithZeroInput() {
        Series series = new Series("0");

        assertTrue(series.isNotValid());
    }

    @Test
    void testSeriesWithTooBigInput() {
        Series series = new Series("56");

        assertTrue(series.isNotValid());
    }

    @Test
    void testSeriesWithValidInput() {
        Series series = new Series("1");

        assertFalse(series.isNotValid());
    }
}