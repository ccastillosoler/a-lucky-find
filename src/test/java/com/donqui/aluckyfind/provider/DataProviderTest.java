package com.donqui.aluckyfind.provider;

import com.donqui.aluckyfind.domain.ticket.Extra;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@TestPropertySource(locations = {"classpath:application-test.properties"})
class DataProviderTest {

    @Autowired
    private DataProvider dataProvider;

    @Test
    void testRetrieveDates() {
        String dates = dataProvider.retrieveDates();

        assertNotNull(dates);
    }

    @Test
    void testRetrieveLastResult() {
        String result = dataProvider.retrieveLastResult();

        assertNotNull(result);
    }

    @Test
    void testRetrievePrizes() {
        String result = dataProvider.retrievePrizes();

        assertNotNull(result);
    }

    @Test
    void testGetResultsWithoutNumber() {
        String result = dataProvider.checkResult(null, "1", Extra.NOT_SELECTED.isSelected(), 0);

        assertEquals(result, "No se ha podido comprobar el resultado del sorteo");
    }

    @Test
    void testGetResultsWithTooBigNumber() {
        String result = dataProvider.checkResult("100000", "1", Extra.NOT_SELECTED.isSelected(), 0);

        assertEquals(result, "No se ha podido comprobar el resultado del sorteo");
    }

    @Test
    void testGetResultsWithoutSerie() {
        String result = dataProvider.checkResult("1", null, Extra.NOT_SELECTED.isSelected(), 0);

        assertEquals(result, "No se ha podido comprobar el resultado del sorteo");
    }

    @Test
    void testGetResultsWithZeroSerie() {
        String result = dataProvider.checkResult("1", "0", Extra.NOT_SELECTED.isSelected(), 0);

        assertEquals(result, "No se ha podido comprobar el resultado del sorteo");
    }

    @Test
    void testGetResultsWithTooBigSerie() {
        String result = dataProvider.checkResult("1", "56", Extra.NOT_SELECTED.isSelected(), 0);

        assertEquals(result, "No se ha podido comprobar el resultado del sorteo");
    }

    @Test
    void testGetResultsWithValidInput() {
        String result = dataProvider.checkResult("12345", "010", Extra.UNKNOWN.isSelected(), 0);

        assertNotEquals(result, "No se ha podido comprobar el resultado del sorteo");
    }
}