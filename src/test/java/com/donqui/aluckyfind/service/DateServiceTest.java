package com.donqui.aluckyfind.service;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;


public class DateServiceTest {

    DateService dateService = new DateService("es-ES");

    @Test
    public void testWithEmptyInput() {
        String parsedDate = dateService.parse(null);

        assertNull(parsedDate);
    }

    @Test
    public void testWithWrongInput() {
        String parsedDate = dateService.parse("01/01/2000");

        assertNull(parsedDate);
    }

    @Test
    public void testWithValidInput() {
        String parsedDate = dateService.parse("lunes, 01 de febrero de 2021");

        assertEquals(parsedDate, "01/02/21");
    }
}